package api

import javax.ws.rs.{POST, Produces, GET, Path}
import javax.ws.rs.core.{UriInfo, Context}


case class Greeting(name:String, msg:String)

case class GreetingWithCollections(names:List[String], msgs:Array[String])

case class GreetingWithOption(name:Option[String], surname:Option[String], msg:String)

case class Link(href:String)

object Link{
  def to(path:String)(implicit uriInfo:UriInfo):Link = Link(uriInfo.getBaseUri.toString + path)
}

@Path("")
@Produces(Array("application/json"))
class GreetingsResource {

  @GET @Path("/")
  def index(@Context uriInfo: UriInfo) = Map("greetings"-> greetingsIndex(uriInfo))

  @GET @Path("/greetings")
  def greetingsIndex(@Context uriInfo: UriInfo) = {
    implicit val uri = uriInfo
    Map(
      "href" -> Link.to("greetings").href,
      "hello" -> Link.to("greetings/hello"),
      "withCollections" -> Link.to("greetings/withCollections"),
      "withOption" -> Link.to("greetings/withOption"))
  }

  @POST @Path("/greetings/")
  def postGreeting(greeting:Greeting) = greeting

  @GET @Path("/greetings/hello")
  def sayHello() = Greeting("Jordi","HELLO!!")

  @GET @Path("/greetings/withCollections")
  def sayHelloWithCollections() = GreetingWithCollections(List("Jordi","John"),Array("HELLO!!","HI!"))

  @GET @Path("/greetings/withOption")
  def sayHelloWithOption() = GreetingWithOption(name = Some("Jordi"), surname=None, msg="HELLO!!")

  @POST @Path("/greetings/withOption")
  def postGreetingWithOption(greeting:GreetingWithOption) = greeting.surname.getOrElse("Got None")


}